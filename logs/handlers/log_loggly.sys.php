<?php
/**
 * @author Ivan Kudryavsky [spam@mr-god.net]
 * @link http://master.virmandy.net
 * @link http://vk.com/id171080259
 *
 * Данный класс является обработчиком логов, отсылающий логи в сервис www.loggly.com
 */
class Log_Loggly extends LogHandler
{
    public $token;
    private $_timeout     = 10;
    const LOGURL          = 'https://logs-01.loggly.com/inputs/%s/tag/%s/';
    const DEBUG           = 'DEBUG';
    const INFO            = 'INFO';
    const WARNING         = 'WARNING';
    const ERROR           = 'ERROR';
    const CRITICAL        = 'CRITICAL';
    const FATAL           = 'FATAL';

    public function send($params,$host,$session,$level,$file,$line,$text,$id,$data)
    {
        $t = microtime(true);
        $this->token = $params["token"];
        $_data=array(
            "file"=>$file,
            "line"=>$line,
            "id"=>$id,
            "timestamp"=>date("Y-m-d H:i:s.").sprintf("%06d",($t - floor($t)) * 1000000)
        );
        if(is_array($data))
            foreach($data as $k=>$v){
                $_data["_".$k]=$v;
            }

        return $this->loglevel($level, $text, $_data);
    }

    public function get_name($lang)
    {
        return "Loggly";
    }

    public function get_description($lang)
    {
        return "Отправка лога в <a href=\"//loggly.com\">Loggly</a>";
    }

    public function get_params($lang){
        return array(
            array("id"=>"token","name"=>"Токен:","required"=>true)
        );
    }

    public function loglevel($level, $message, $data = null) {
        if (!is_array($data)) $data = array();
        $data['message']  = $message;
        $data['severity'] = $level;
        return $this->log($data);
    }

    public function log($data) {
        if (!is_array($data)) throw new Exception("Invalid params", 1);
        if (!isset($data['message'])) throw new Exception("No message given to log", 1);
        $data['timestamp'] = isset($data['timestamp']) ? $data['timestamp'] : date('c'); // default timestamp if missing
        if (!isset($data['tags'])) {
            $tags_list = 'http';
        } else {
            if (!is_array($data['tags'])) $data['tags'] = array($data['tags']); // convert to array
            $tags_list = implode(',', $data['tags']);
        }

        $s = curl_init();
        curl_setopt($s, CURLOPT_URL, sprintf(self::LOGURL, $this->token, $tags_list));
        curl_setopt($s, CURLOPT_HTTPHEADER, array('Expect:'));
        curl_setopt($s, CURLOPT_TIMEOUT, $this->_timeout);
        curl_setopt($s, CURLOPT_MAXREDIRS, 2);
        curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($s, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($s, CURLOPT_POST, true);
        curl_setopt($s, CURLOPT_POSTFIELDS, json_encode($data,JSON_UNESCAPED_UNICODE));
        curl_setopt($s, CURLOPT_HTTPHEADER, array('content-type:application/x-www-form-urlencoded'));
        $json_string = curl_exec($s);
        $status = curl_getinfo($s, CURLINFO_HTTP_CODE);
        curl_close($s);
        if ($status != 200) {
            throw new Exception("Failed to log http[$status]", 1);
        }
        $json = json_decode($json_string);
        if (isset($json->response) && $json->response == 'ok') {
            return true;
        }
        return false;
    }

}