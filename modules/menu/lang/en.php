<?php

$type_langauge = 'en';

$il['menu_name_modul_base_name'] = 'Menu';
$il['menu_name_modul_base_name1'] = 'Menu';

$il['module_menu_label_header'] = 'Title menu';
$il['module_menu_label_propertes1'] = 'Template menu';
$il['module_menu_visible_var1'] = 'Show';
$il['module_menu_visible_var2'] = 'Do not display';

$il['menu_show_menu'] = 'Generate dynamic menu';
$il['menu_show_menu_static'] = 'Generate static menu';
$il['module_menu_id_page_start1'] = 'Page start of construction';
$il['module_menu_count_level_start'] = 'The level of the beginning of construction';
$il['module_menu_count_level_show'] = 'Number of displayed menu levels';

$il['module_menu_l_id_page_start'] = 'Page on the beginning of postoroeniya';
$il['module_menu_label_visible'] = 'Visibility page';

?>