<?php
$type_langauge							= 'en';

$il['gallery_modul_base_name']          = 'Gallery';
$il['gallery_modul_base_name1']          = 'Gallery';
$il['gallery_property_img_source_width']  = 'The width of the original image';
$il['gallery_property_img_source_height']  = 'The height of the original image';
$il['gallery_property_img_big_width']  = 'The width of the big picture';
$il['gallery_property_img_big_height']  = 'Height larger image';
$il['gallery_property_img_small_width']  = 'The width of the small picture';
$il['gallery_property_img_small_height']  = 'The height of the small picture';
$il['gallery_list_delete_element_alert']    = 'The image will be deleted. Continue?';
$il['gallery_pub_create_content']       = 'Display the contents of the gallery';
$il['gallery_module_pub1_propertes3']  = 'Template gallery';

$il['gallery_property_path_to_copyright_file'] = 'File watermark';
$il['gallery_property_copyright_transparency'] = 'Transparency watermark';

$il['gallery_property_add_copyright'] = 'Add watermark?';
$il['gallery_property_copyright_position'] = 'Location watermark';
$il['gallery_property_copyright_position_0'] = 'In the center';
$il['gallery_property_copyright_position_1'] = 'Top-left';
$il['gallery_property_copyright_position_2'] = 'Top-right';
$il['gallery_property_copyright_position_3'] = 'Bottom-right';
$il['gallery_property_copyright_position_4'] = 'Bottom-left';


$il['gallery_pub_random_photos'] = 'Random images';
$il['gallery_items_per_page'] = 'Items per page';
$il['gallery_items_to_show'] = 'Number of output images';
$il['gallery_pub_show_photos_sort'] = 'Display the contents of the category';

$il['gallery_module_label_block_menu'] = 'Basic steps';
$il['gallery_menu_photos'] = 'Image';
$il['gallery_menu_add'] = 'Add a picture';
$il['gallery_menu_cats'] = 'Categories';

$il['gallery_menu_custom_fields'] = 'Custom fields';
$il['gallery_custom_fields_empty'] = '-not selected-';
$il['custom fields field title'] = 'Title field';
$il['custom_fields_field_name'] = 'Field ID';
$il['custom_fields_field_type'] = 'Field type';
$il['custom_fields_field_type_string'] = 'String';
$il['custom_fields_field_type_textarea'] = 'Textarea';
$il['custom_fields_field_type_select'] = 'A set of values (ENUM)';
$il['custom_fields_field_type_checkbox'] = 'A set of values (SET)';
$il['custom_fields_field_type_fileselect'] = 'Select file';
$il['custom_fields_field_type_imageselect'] = 'Select image';
$il['custom_fields_field_type_label'] = 'Value importance with new line';
$il['custom_fields_field_new'] = 'New custom field';
$il['custom_fields_field_edit'] = 'Edit custom field';
$il['custom_fields_field_order'] = 'Sorting';
$il['custom_fields_order_delete'] = 'Remove selected';
$il['gallery_custom_field_delete_confirm'] = 'Indeed delete custom field right? All value will drop.';
$il['custom_fields_order_save'] = 'Save order';
$il['custom_fields_order_add'] = 'Add field';

$il['gallery_save'] = 'Save';
$il['gallery_edit'] = 'Edit';
$il['gallery_delete'] = 'Remove';
$il['gallery_submit'] = 'Submit';
$il['gallery_items_select_action'] = 'Select action:';
$il['gallery_items_count'] = 'Count images: ';
$il['gallery_delete_items'] = 'Delete images';
$il['gallery_select_all_items'] = ' - process all image categories';

$il['gallery_add_category'] = 'Add category';
$il['gallery_add_delete_category'] = 'Add / Edit category';
$il['gallery_list_category'] = 'List of categories';

$il['gallery_delete_category'] = 'Delete';
$il['gallery_category_name'] = 'Title';
$il['gallery_category_delete_alert'] = 'Really delete the category (images from it will not be deleted)';

$il['gallery_imageform_header'] = 'Image Editing';
$il['gallery_imageform_category'] = 'Category';
$il['gallery_imageform_category_empty'] = '-do not selected (no category)-';
$il['gallery_list_all_cats'] = '-all category-';

$il['gallery_import_archive'] = 'Import archive';
$il['gallery_import_archive_title'] = 'Import archive with images';
$il['gallery_import_upload_file'] = 'Download archive';
$il['gallery_import_file_on_server'] = 'From the archive server';
$il['gallery_import_from'] = 'Imports from';
$il['gallery_import_gen_names'] = 'Image title';
$il['gallery_import_gen_names_filename'] = 'The image file name';
$il['gallery_import_gen_names_number'] = 'Serial numbers (1..N)';
$il['gallery_import_upload_failed'] = 'Error loading file';
$il['gallery_import_archive_read_failed'] = 'Error reading archive';
$il['gallery_import_archive_put_failed'] = 'Failed to save the image file';
$il['gallery_import_added'] = 'Added images:';
$il['gallery_category_descr'] = 'Category description';

$il['gallery_pub_categories_list'] = 'Show categories list';
$il['gallery_template_list'] = 'Categories list template';
$il['gallery_gallery_page'] = 'Gallery page';

$il['gallery_items_sorting'] ='Sort by:';
$il['gallery_order_num'] ='Sort order';
$il['gallery_order_date'] ='Date';
$il['gallery_order_ID'] ='ID';
$il['gallery_describe'] ='Description';
$il['gallery_operation'] ='Management';
$il['gallery_property_sort_ask'] ='ID ascending';
$il['gallery_property_sort_desk'] ='ID descending';
$il['gallery_property_sort_order_num_ask'] ='In the field Sort Ascending';
$il['gallery_property_sort_order_num_desk'] ='In the field Sort Descending';
$il['gallery_list_photos'] ='List of images';
$il['gallery_file'] = 'File';
$il['gallery_image_name'] = 'Image title';
$il['gallery_update_image'] = 'Refresh Image';
$il['gallery_image'] = 'Image';

?>