<?php

$type_langauge = 'ua';

$il['gallery_modul_base_name'] = 'Галерея';
$il['gallery_modul_base_name1'] = 'Галерея';
$il['gallery_property_img_source_width'] = 'Ширина вихідного зображення';
$il['gallery_property_img_source_height'] = 'Висота вихідного зображення';
$il['gallery_property_img_big_width'] = 'Ширина великого зображення';
$il['gallery_property_img_big_height'] = 'Висота великої зображення';
$il['gallery_property_img_small_width'] = 'Ширина маленького зображення';
$il['gallery_property_img_small_height'] = 'Висота маленького зображення';
$il['gallery_list_delete_element_alert'] = 'Зображення буде видалено. Продовжити?';
$il['gallery_pub_create_content'] = 'Вивести вміст галереї';
$il['gallery_module_pub1_propertes3'] = 'Шаблон галереї';

$il['gallery_property_path_to_copyright_file'] = 'Файл водяного знака';
$il['gallery_property_copyright_transparency'] = 'Прозорість водяного знака';

$il['gallery_property_add_copyright'] = 'Додати водяний знак?';
$il['gallery_property_copyright_position'] = 'Розташування водяного знака';
$il['gallery_property_copyright_position_0'] = 'По центру';
$il['gallery_property_copyright_position_1'] = 'Зверху-зліва';
$il['gallery_property_copyright_position_2'] = 'Зверху-праворуч';
$il['gallery_property_copyright_position_3'] = 'Знизу-праворуч';
$il['gallery_property_copyright_position_4'] = 'Знизу-зліва';

$il['gallery_pub_random_photos'] = 'Випадкові зображення';
$il['gallery_items_per_page'] = 'Елементів на сторінку';
$il['gallery_items_to_show'] = 'Кількість виведених зображень';
$il['gallery_pub_show_photos_sort'] = 'Вивести вміст категорії';

$il['gallery_module_label_block_menu'] = 'Основні дії';
$il['gallery_menu_photos'] = 'Зображення';
$il['gallery_menu_add'] = 'Додати зображення';
$il['gallery_menu_cats'] = 'Категорії';

$il['gallery_menu_custom_fields'] = 'Довільні Поля';
$il['gallery_custom_fields_empty'] = '-не вибрано-';
$il['custom_fields_field_title'] = 'Назва поля';
$il['custom_fields_field_name'] = 'Ідентифікатор';
$il['custom_fields_field_type'] = 'Тип поля';
$il['custom_fields_field_type_string'] = 'Рядок';
$il['custom_fields_field_type_textarea'] = 'Текстове';
$il['custom_fields_field_type_select'] = 'Набір значень (ENUM)';
$il['custom_fields_field_type_checkbox'] = 'Набір значень (SET)';
$il['custom_fields_field_type_fileselect'] = 'Выбір файлу';
$il['custom_fields_field_type_imageselect'] = 'Выбір зображення';
$il['custom_fields_field_type_label'] = 'Кожне значення з нового рядка';
$il['custom_fields_field_new'] = 'Нове довільне поле';
$il['custom_fields_field_edit'] = 'Редагування довільного поля';
$il['custom_fields_field_order'] = 'Порядок сортування';
$il['custom_fields_order_delete'] = 'Видалити вибрані';
$il['gallery_custom_field_delete_confirm'] = 'Дійсно видалити довільне поле? Всі значення будуть втрачені.';
$il['custom_fields_order_save'] = 'Зберегти порядок';
$il['custom_fields_order_add'] = 'Додати поле';

$il['gallery_save'] = 'Зберегти';
$il['gallery_edit'] = 'Редагувати';
$il['gallery_delete'] = 'Видалити';
$il['gallery_submit'] = 'Виконати';
$il['gallery_items_select_action'] = 'Дія з обраними:';
$il['gallery_items_count'] = 'Всього зображень: ';
$il['gallery_delete_items'] = 'Видалити зображення';
$il['gallery_select_all_items'] = ' - обробка всіх зображень';

$il['gallery_add_category'] = 'Додати категорію';
$il['gallery_add_delete_category'] = 'Додати / редагувати категорію';
$il['gallery_list_category'] = 'Cписок категорій';

$il['gallery_delete_category'] = 'Видалити';
$il['gallery_category_name'] = 'Назва';
$il['gallery_category_delete_alert'] = 'Дійсно видалити категорію (зображення з неї НЕ будуть видалені)';

$il['gallery_imageform_header'] = 'Редагування зображення';
$il['gallery_imageform_category'] = 'Категорія';
$il['gallery_imageform_category_empty'] = '-не вибрано (без категорії) -';
$il['gallery_list_all_cats'] = '-всі категорії-';

$il['gallery_import_archive'] = 'Імпорт архіву';
$il['gallery_import_archive_title'] = 'Імпорт архіву з зображеннями';
$il['gallery_import_upload_file'] = 'Завантажити архів';
$il['gallery_import_file_on_server'] = 'З архіву на сервері';
$il['gallery_import_from'] = 'Імпорт з';
$il['gallery_import_gen_names'] = 'Назви зображень';
$il['gallery_import_gen_names_filename'] = 'Файл зображення';
$il['gallery_import_gen_names_number'] = 'Порядкові номери (1..N)';
$il['gallery_import_upload_failed'] = 'Помилка при завантаженні файлу';
$il['gallery_import_archive_read_failed'] = 'Помилка читання архіву';
$il['gallery_import_archive_put_failed'] = 'Помилка збереження файлу зображення';
$il['gallery_import_added'] = 'Додано зображень:';
$il['gallery_category_descr'] = 'Опис категорії';

$il['gallery_pub_categories_list'] = 'Відобразити категорій галереї';
$il['gallery_template_list'] = 'Шаблон списку категорій';
$il['gallery_gallery_page'] = 'Сторінка для відображеня галереї';

$il['gallery_items_sorting'] ='Сортування по:';
$il['gallery_order_num'] ='Порядок сортування';
$il['gallery_order_date'] ='Дата';
$il['gallery_order_ID'] ='ID';
$il['gallery_describe'] ='Опис';
$il['gallery_operation'] ='Управління';
$il['gallery_property_sort_ask'] ='ID по зростанню';
$il['gallery_property_sort_desk'] ='ID спаданням';
$il['gallery_property_sort_order_num_ask'] ='По полю Порядок сортування за зростанням';
$il['gallery_property_sort_order_num_desk'] ='По полю Порядок сортування за спаданням';
$il['gallery_list_photos'] ='Список зображень';

$il['gallery_file'] = 'Файл';
$il['gallery_image_name'] = 'Назва зображення';
$il['gallery_update_image'] = 'Оновити зображення';
$il['gallery_image'] = 'Зображення';

?>