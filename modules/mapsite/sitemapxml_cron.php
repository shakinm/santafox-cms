<?php
$webRoot= dirname(dirname(dirname(__FILE__)))."/";
chdir($webRoot);
require_once $webRoot."ini.php";
require_once $webRoot."include/kernel.class.php";
include ($webRoot."include/security.class.php"); //Безопасность
require_once $webRoot."modules/mapsite/mapsite.class.php";
require_once $webRoot."admin/manager_modules.class.php";
$kernel = new kernel(PREFIX);
$site_root = $kernel->pub_site_root_get();
$kernel->priv_module_for_action_set('mapsite1');
$mapsite = new mapsite();
$mapsite->pub_create_sitemapxml();
$mapsiteFile = $site_root.'/sitemap.xml';
header('Content-type: application/xml; charset=utf-8');
readfile($mapsiteFile);