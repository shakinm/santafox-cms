<?php

/**
 * Модуль "Обратная связь"
 *
 * @author Александр Ильин mecommayou@gmail.com
 * @version 1.0 beta
 *
 */
class feedback_install extends install_modules
{
	/**
	 * Инсталяция базового модуля
	 *
	 * @param string $id_module Идентификатор создаваемого базового модуля
     * @param boolean $reinstall переинсталяция?
	 */
	function install($id_module, $reinstall = false)
	{
        global $kernel;
        $query = "CREATE TABLE IF NOT EXISTS `".PREFIX."_".$id_module."_messages` ( "
			. " `id` int(10) unsigned NOT NULL AUTO_INCREMENT, "
			. " `module_id` varchar(255) NOT NULL, "
			. " `name` varchar(255) NOT NULL, "
			. " `email` varchar(255) NOT NULL, "
			. " `theme` varchar(255) NOT NULL, "
			. " `message` text, "
			. " `pubdate` datetime NOT NULL, "
			. " `status` enum('success','error') NOT NULL, "
			. " PRIMARY KEY  (`id`), "
			. " KEY `module_id` (`module_id`) "
		. " ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
        $kernel->runSQL($query);
	}

	/**
     * Деинсталяция базового модуля
     *
     * @param string $id_module Идентификатор удаляемого базового модуля
     */
	function uninstall($id_module)
	{
		global $kernel;
        $query = "DROP TABLE `".PREFIX."_".$id_module."_messages`";
        $kernel->runSQL($query);
	}

	/**
     * Инсталяция дочернего модуля
     *
     * @param string $id_module Идентификатор вновь создаваемого дочернего модуля
     * @param boolean $reinstall переинсталяция?
     */
	function install_children($id_module, $reinstall = false)
	{
	}

	/**
	 * Деинсталяция дочернего модуля
	 *
     *
     * @param string $id_module ID удоляемого дочернего модуля
     */
	function uninstall_children($id_module)
	{
	}
}

$install = new feedback_install();
$install->set_name('[#feedback_modul_base_name#]');
$install->set_id_modul('feedback');
$install->set_admin_interface(2);

// Элементов на страницу для админа
$property = new properties_string();
$property->set_caption('[#feedback_property_messages_perpage#]');
$property->set_default('10');
$property->set_id('feedback_messages_perpage');
$install->add_modul_properties($property);

$install->add_public_metod('pub_show_form', '[#feedback_pub_show_form#]');

$p = new properties_file();
$p->set_id('template');
$p->set_caption('[#feedback_property_label_template#]');
$p->set_patch('modules/feedback/templates_user');
$p->set_mask('htm,html');
$p->set_default('modules/feedback/templates_user/default.html');
$install->add_public_metod_parametrs('pub_show_form',$p);

$p = new properties_string();
$p->set_id('email');
$p->set_caption('[#feedback_property_label_email#]');
$p->set_default(isset($_SERVER['SERVER_ADMIN'])?$_SERVER['SERVER_ADMIN']:'');
$install->add_public_metod_parametrs('pub_show_form',$p);

$p = new properties_select();
$p->set_id('type');
$p->set_caption('[#feedback_property_label_type#]');
$p->set_data(array("html"=>"[#feedback_property_label_html#]", "text"=>"[#feedback_property_label_text#]"));
$p->set_default('text');
$install->add_public_metod_parametrs('pub_show_form',$p);

$p = new properties_string();
$p->set_id('name');
$p->set_caption('[#feedback_property_label_name#]');
$p->set_default('');
$install->add_public_metod_parametrs('pub_show_form',$p);

$p = new properties_string();
$p->set_id('theme');
$p->set_caption('[#feedback_property_label_theme#]');
$p->set_default('Обращение через форму обратной связи');
$install->add_public_metod_parametrs('pub_show_form',$p);


$install->module_copy[0]['name'] = 'feedback_modul_base_name';

//$install->module_copy[0]['macros'][0]['caption']    = '[#feedback_pub_show_form#]';
//$install->module_copy[0]['macros'][0]['id_metod']   = 'pub_show_form';

//$install->module_copy[0]['macros'][0]['properties']['template'] = 'modules/feedback/templates_user/default.html';
//$install->module_copy[0]['macros'][0]['properties']['email']    = $_SERVER['SERVER_ADMIN'];