<?php

$type_langauge = 'ru';

$il['news_base_name']         = 'Новости';
$il['newsi_modul_base_name1'] = 'Основные новости';

$il['news_property_img_source_width']   = 'Ширина исходной картинки';
$il['news_property_img_source_height']   = 'Высота исходной картинки';
$il['news_property_img_big_width']      = 'Ширина большой картинки в пикселях';
$il['news_property_img_big_height']     = 'Высота большой картинки в пикселях';
$il['news_property_img_small_width']    = 'Ширина маленькой картинки в пикселях';
$il['news_property_img_small_height']   = 'Высота маленькой картинки в пикселях';
$il['news_property_deliver']            = 'Значение признака "В рассылку"';
$il['news_property_lenta']              = 'Значение признака "В ленту"';
$il['news_property_rss']                = 'Значение признака "В RSS"';
$il['news_property_news_per_page']      = 'Новостей на страницу для АИ';
$il['news_property_page_for_lenta']     = 'Страница где формируется архив (заполняется в том случае, к новостям модуля обращаются другие новостные ленты)';
$il['news_property_copyright_transparency'] = 'Прозрачность watermark';

$il['news_pub_show_lenta']              = 'Сформировать ленту';
$il['news_pub_show_lenta_template']     = 'Шаблон ленты';
$il['news_pub_show_lenta_limit']        = 'Количество новостей в ленте';
$il['news_pub_show_lenta_type']         = 'Тип отбора';
$il['news_pub_show_lenta_type_default'] = 'По умолчанию';
$il['news_pub_show_lenta_type_past']    = 'От текущей даты в прошлое';
$il['news_pub_show_lenta_type_future']  = 'От текущей даты в будующее';
$il['news_pub_show_lenta_page']         = 'Страница где формируется архив';
$il['news_pub_show_lenta_id_modules']   = 'Идентификаторы модулей (через запятую), чьи новости так же попадают в ленту';

$il['news_property_watermark']          = 'Ставить ли водяной знак?';
$il['news_property_path_to_copyright_file'] = 'Выберите водяной знак';
$il['news_property_copyright_position'] = 'Расположение водяного знака';
$il['news_property_copyright_position_0'] = 'По центру';
$il['news_property_copyright_position_1'] = 'Сверху-слева';
$il['news_property_copyright_position_2'] = 'Сверху-справа';
$il['news_property_copyright_position_3'] = 'Снизу-справа';
$il['news_property_copyright_position_4'] = 'Снизу-слева';

$il['news_pub_show_archive']            = 'Сформировать архив';
$il['news_pub_show_archive_template']   = 'Шаблон';
$il['news_pub_show_archive_limit']      = 'Кол-во новостей на страницу';
$il['news_pub_show_archive_type']       = 'Тип отбора';
$il['news_pub_show_archive_default']    = 'Независимо от даты';
$il['news_pub_show_archive_past']       = 'От текущей даты в прошлое';
$il['news_pub_show_archive_future']     = 'От текущей даты в будующее';
$il['news_pub_show_lenta_type_random']  = 'Случайная запись';

$il['news_pub_show_sorting']            = 'Показать сортировки';
$il['news_pub_show_selection']          = 'Показать отбор';
$il['news_pub_show_selection_template'] = 'Шаблон';

$il['news_menu_label']                  = 'Управление';
$il['news_menu_show_list']              = 'Просмотреть';
$il['news_menu_between']                = 'За период';
$il['news_menu_add_new']                = 'Добавить';

$il['news_show_list_action_lenta_on']       = 'Отображать в ленте';
$il['news_show_list_action_lenta_off']      = 'Не отображать в ленте';
$il['news_show_list_action_available_on']   = 'Сделать видимым';
$il['news_show_list_action_available_off']  = 'Сделать не видимым';
$il['news_show_list_action_rss_on']         = 'Отображать в RSS';
$il['news_show_list_action_rss_off']        = 'Не отображать в RSS';
$il['news_show_list_action_delete']         = 'Удалить';
$il['news_show_list_action_move']           = 'Переместить';

// Заголовки в show_list
$il['news_item_id']         = 'ID';
$il['news_item_date']       = 'Дата';
$il['news_item_datetime']   = 'Дата и время';
$il['news_item_header']     = 'Заголовок';
$il['news_item_available']  = 'Видимость';
$il['news_item_lenta']      = 'В ленте';
$il['news_item_rss']        = 'В RSS';
$il['news_item_author']     = 'Автор';
$il['news_item_actions']    = 'Действия';

$il['news_property_date_label']                 = 'Дата публикации';
$il['news_property_time_label']                 = 'Время публикации';
$il['news_property_available_label']            = 'Новость активна';
$il['news_property_lenta_label']                = 'Отображать в ленте';
$il['news_property_delivery_label']             = 'Отображать в рассылке';
$il['news_property_rss_label']                  = 'Отображать в RSS';
$il['news_property_header_label']               = 'Заголовок';
$il['news_property_description_short_label']    = 'Краткое описание';
$il['news_property_description_full_label']     = 'Полное описение';
$il['news_property_author_label']               = 'Автор';
$il['news_property_source_name_label']          = 'Имя источника';
$il['news_property_source_url_label']           = 'Адрес URL';
$il['news_property_image_label']                = 'Изображение';
$il['news_property_news_date_format']           = 'Формат даты (d.m.Y)';

$il['news_item_action_edit']        = 'Редактировать';
$il['news_item_action_remove']      = 'Удалить';
$il['news_show_list_submit']        = 'ОК';
$il['news_actions_with_selected']   = 'Действия с отмеченными:';
$il['news_submit_label']            = 'Сохранить';
$il['news_actions_simple']          = 'Действия';
$il['news_actions_advanced']        = 'Переместить в:';
$il['news_item_number']             = 'Номер';
$il['news_menu_label1']             = 'Отбор по дате';
$il['news_select_between_label']    = 'Укажите желаемый диапазон дат';
$il['news_start_date']              = 'Укажите желаемый диапазон дат';
$il['news_end_date']                = 'Укажите желаемый диапазон дат';
$il['news_button_show']             = 'Отобразить';


$il['news_property_pages_count']      = 'Количество страниц в блоке (N)';
$il['news_pub_pages_type']            = 'Вид постраничной навигации';
$il['news_pub_pages_get_block']       = 'Блоками по N страниц';
$il['news_pub_pages_get_float']       = 'Тек. страница всегда в центре блока из N страниц';
$il['news_delete_confirm']            = 'Вы действительно хотите удалить новость?';

$il['news_pub_show_html_title']       = 'Вывести заголовок';
$il['news_pub_show_meta_keywords']    = 'Вывести meta-keywords';
$il['news_pub_show_meta_description'] = 'Вывести meta-description';
$il['news_pub_pub_show_html_title_def'] = 'Заголовок по-умолчанию';
$il['news_pub_pub_show_meta_keywords_def'] = 'Meta-keywords по-умолчанию';
$il['news_pub_pub_show_meta_description_def'] = 'Meta-description по-умолчанию';
$il['news_property_html_title_label']       = 'SEO: Заголовок';
$il['news_property_meta_keywords_label']    = 'SEO: Meta-keywords';
$il['news_property_meta_description_label'] = 'SEO: Meta-description';

$il['news_error_incorrect_datetime'] 		= 'Некорректное значение даты или времени';
