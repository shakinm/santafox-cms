<?php
$type_langauge = 'en';

$il['contents_modul_base_name'] = 'content blocks';
$il['contents_modul_base_name1'] = 'Block content';

$il['contents_block_add_edit'] = 'Add/edit unit';
$il['contents_block_add'] = 'Add Block';
$il['contents_block_name'] = 'Block header';
$il['contents_block_descr'] = 'Description';
$il['contents_items_count'] = 'Number';

$il['content_fields_field_title'] = 'Name of the field';
$il['content_fields_field_name'] = 'Identifier';
$il['content_fields_field_type'] = 'Field type';
$il['content_fields_field_type_string'] = 'String';
$il['content_fields_field_type_textarea'] = 'Text';
$il['content_fields_field_type_editor'] = 'Edit';
$il['content_fields_field_type_select'] = 'Set values ​​(ENUM)';
$il['content_fields_field_type_checkbox'] = 'Set values ​​(SET)';
$il['content_fields_field_type_fileselect'] = 'Select file';
$il['content_fields_field_type_imageselect'] = 'Selecting the picture';
$il['content_fields_field_type_label'] = 'Each value on a new line';
$il['content_fields_field_new'] = 'New custom field';
$il['content_fields_field_edit'] = 'Edit the field';
$il['content_fields_field_id'] = 'ID';
$il['content_fields_field_date'] = 'Date';
$il['content_fields_field_order'] = 'Sort order';
$il['content_fields_delete'] = 'Remove selected';
$il['content_block_delete_confirm'] = 'Are you sure you want to remove the content block?';
$il['content_fields_delete_confirm'] = 'Are you sure you want to delete the content field?';
$il['content_item_delete_confirm'] = 'Are you sure you want to delete the selected content?';
$il['content_fields_save'] = 'Save order';
$il['content_fields_add'] = 'Add Field';
$il['content_fields_edit'] = 'Edit the field';

$il['contents_menu_block_label'] = 'Key actions';
$il['contents_menu_blocks'] = 'Block of contents';
$il['contents_menu_fields'] = 'Field list';
$il['content_fields_field_new'] = 'New content field';

$il['pub_show_content'] = 'Show the contents of the block';
$il['contents_template'] = 'Template';
$il['contents_select_blocks'] = 'Block content';
$il['contents_property_sort_ask'] = 'ID ascending';
$il['contents_property_sort_desk'] = 'ID descending';
$il['contents_property_sort_order_num_ask'] = 'Sort order ascending';
$il['contents_property_sort_order_num_desk'] = 'Sort by descending';

$il['contents_list_blocks'] = 'List of content blocks';
$il['contents_list_contents'] = 'The block';
$il['contents_list_selectall'] = 'Select All';
$il['contents_content'] = 'Short contents';

$il['contents_form_header_add'] = 'Add content';
$il['contents_form_header_edit'] = 'Content editing';

$il['contents_content_fields_empty'] = '- not selected -';
$il['content_block_not_set'] = '- not selected -';
$il['contents_add'] = 'Add content';
$il['contents_save'] = 'Save';
$il['contents_edit'] = 'Edit';
$il['contents_delete'] = 'Delete';
$il['contents_select_action'] = 'Select an action';
$il['contents_delete_selected'] = 'Remove selected';
$il['contents_delete_all'] = 'Delete a block of content';
