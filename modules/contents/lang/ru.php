<?php
$type_langauge = 'ru';

$il['contents_modul_base_name'] = 'Блоки контента';
$il['contents_modul_base_name1'] = 'Блок контента';

$il['contents_block_add_edit'] = 'Добавить/редактировать блок';
$il['contents_block_add'] = 'Добавить блок';
$il['contents_block_name'] = 'Заголовок блока';
$il['contents_block_descr'] = 'Описание блока';
$il['contents_items_count'] = 'Количество';

$il['content_fields_field_title'] = 'Название поля';
$il['content_fields_field_name'] = 'Идентификатор';
$il['content_fields_field_type'] = 'Тип поля';
$il['content_fields_field_type_string'] = 'Строка';
$il['content_fields_field_type_textarea'] = 'Текстовое';
$il['content_fields_field_type_editor'] = 'Редактор';
$il['content_fields_field_type_select'] = 'Набор значений (ENUM)';
$il['content_fields_field_type_checkbox'] = 'Набор значений (SET)';
$il['content_fields_field_type_fileselect'] = 'Выбор файла';
$il['content_fields_field_type_imageselect'] = 'Выбор изображения';
$il['content_fields_field_type_label'] = 'Каждое значение с новой строки';
$il['content_fields_field_new'] = 'Новое произвольное поле';
$il['content_fields_field_edit'] = 'Редактирование поля';
$il['content_fields_field_id'] = 'ID';
$il['content_fields_field_date'] = 'Дата добавления';
$il['content_fields_field_order'] = 'Порядок сортировки';
$il['content_fields_delete'] = 'Удалить выбранные';
$il['content_block_delete_confirm'] = 'Вы действительно желаете удалить блок контента?';
$il['content_fields_delete_confirm'] = 'Вы действительно желаете удалить поле контента?';
$il['content_item_delete_confirm'] = 'Вы действительно желаете удалить выбранный контент?';
$il['content_fields_save'] = 'Сохранить порядок';
$il['content_fields_add'] = 'Добавить поле';
$il['content_fields_edit'] = 'Редактировать поле';

$il['contents_menu_block_label'] = 'Основные действия';
$il['contents_menu_blocks'] = 'Блоки контента';
$il['contents_menu_fields'] = 'Список полей';
$il['content_fields_field_new'] = 'Новое поле контента';

$il['pub_show_content'] = 'Показать содержимое блока';
$il['contents_template'] = 'Шаблон вывода';
$il['contents_select_blocks'] = 'Блок-контента';
$il['contents_property_sort_ask'] ='ID по возрастанию';
$il['contents_property_sort_desk'] ='ID по убыванию';
$il['contents_property_sort_order_num_ask'] ='Порядок сортировки по возрастанию';
$il['contents_property_sort_order_num_desk'] ='Порядок сортировки по убыванию';

$il['contents_list_blocks'] = 'Список блоков контента';
$il['contents_list_contents'] = 'Содержимое блока';
$il['contents_list_selectall'] = 'Выбрать все';
$il['contents_content'] = 'Краткое содержимое';

$il['contents_form_header_add'] = 'Добавление контента';
$il['contents_form_header_edit'] = 'Редактирование контента';

$il['contents_content_fields_empty'] = '-- не выбрано --';
$il['content_block_not_set'] = '-- не выбрано --';
$il['contents_add'] = 'Добавить контент';
$il['contents_save'] = 'Сохранить';
$il['contents_edit'] = 'Редактировать';
$il['contents_delete'] = 'Удалить';
$il['contents_select_action'] = 'Выберите действие:';
$il['contents_delete_selected'] = 'Удалить выбранные';
$il['contents_delete_all'] = 'Очистить блок от содержимого';
