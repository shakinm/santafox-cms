<?php

$type_langauge = 'ua';

$il['comments_base_name'] = 'Коментарі та відгуки';
$il['comments_modul_base_name1'] = 'Коментарі та відгуки';

$il['comments_property_admin_email'] = 'Email адміна';
$il['comments_property_premod'] = 'Премодерація?';
$il['comments_property_showcaptcha'] = 'Показувати каптчу?';
$il['comments_property_comments_per_page_admin'] = 'Коментарів на сторінку для АІ';
$il['comments_property_news_date_format'] = 'Формат дати';

$il['comments_pub_show_comments'] = 'Показати коментарі';
$il['comments_pub_show_comments_template'] = 'Шаблон';
$il['comments_pub_show_comments_httpparams'] = 'http-параметри';
$il['comments_pub_show_comments_limit'] = 'Коментарів на сторінку';
$il['comments_pub_show_comments_type'] = 'Сортування';
$il['comments_pub_show_comments_type_default'] = 'За замовчуванням';
$il['comments_pub_show_comments_type_new_at_top'] = 'Нові зверху';
$il['comments_pub_show_comments_type_new_at_bottom'] = 'Нові знизу';
$il['comments_pub_show_page_reviews']    = 'Показати відгуки з сторінки';
$il['comments_pub_show_comments_pagereviews'] = 'Сторінка з відгуками';

$il['comments_pub_show_sorting'] = 'Показати сортування';
$il['comments_pub_show_selection'] = 'Показати відбір';
$il['comments_pub_show_selection_template'] = 'Шаблон';

$il['comments_menu_label'] = 'Коментарі';
$il['comments_menu_show_list'] = 'Переглянути';
$il['comments_menu_between'] = 'За період';
$il['comments_menu_notmoderated'] = 'Чи не отмодерірованние';
$il['comments_menu_add_new'] = 'Додати';

$il['comments_property_photo_label'] = 'Фото автора';
$il['comments_property_no_photo'] = 'Немає фото';

$il['comments_property_img_source_width'] = 'Ширина вихідної картинки';
$il['comments_property_img_source_height'] = 'Висота вихідної картинки';
$il['comments_property_img_big_width'] = 'Ширина великий картинки в пікселях';
$il['comments_property_img_big_height'] = 'Висота великої картинки в пікселях';
$il['comments_property_img_small_width'] = 'Ширина маленької картинки в пікселях';
$il['comments_property_img_small_height'] = 'Висота маленької картинки в пікселях';

$il['comments_show_list_action_available_on'] = 'Позначити як модерувати';
$il['comments_show_list_action_available_off'] = 'Позначити як немодерірованние';
$il['comments_show_list_action_delete'] = 'Видалити';
$il['comments_show_list_action_move'] = 'Перемістити';

// Заголовки в show_list
$il['comments_item_id'] = 'ID';
$il['comments_item_date'] = 'Дата';
$il['comments_item_pages'] = 'Сторінка'; // додано aim
$il['comments_item_txt'] = 'Текст';
$il['comments_item_available'] = 'Видимість';
$il['comments_item_author'] = 'Автор';
$il['comments_item_actions'] = 'Дії';

$il['comments_property_date_label'] = 'Дата';
$il['comments_property_time_label'] = 'Час (ГГ:ХХ:СС)';
$il['comments_property_pages_label'] = 'Сторінка додавання'; // додано aim
$il['comments_property_edit_label'] = 'Сторінка редагування'; // додано aim
$il['comments_property_available_label'] = 'Коментар активний';
$il['comments_property_txt_label'] = 'Текст коментаря';
$il['comments_property_author_label'] = 'Автор';

$il['comments_item_action_edit'] = 'Редагувати';
$il['comments_item_action_remove'] = 'Видалити';
$il['comments_show_list_submit'] = 'ОК';
$il['comments_actions_with_selected'] = 'Дії з зазначеними коментарями:';
$il['comments_submit_label'] = 'Зберегти';
$il['comments_actions_simple'] = 'Дії';
$il['comments_actions_advanced'] = 'Перемістити в:';
$il['comments_item_number'] = '#';
$il['comments_menu_label1'] = 'Відбір за датою';
$il['comments_select_between_label'] = 'Вкажіть бажаний діапазон дат';
$il['comments_start_date'] = 'Вкажіть бажаний діапазон дат';
$il['comments_end_date'] = 'Вкажіть бажаний діапазон дат';
$il['comments_button_show'] = 'Показати';


$il['comments_property_pages_count'] = 'Кількість сторінок в блоці (N)';
$il['comments_pub_pages_type'] = 'Вид посторінковою навігації';
$il['comments_pub_pages_get_block'] = 'Блоками по N сторінок';
$il['comments_pub_pages_get_float'] = 'Тек. сторінка завжди в центрі блоку з N сторінок ';
$il['comments_delete_confirm'] = 'Ви дійсно хочете видалити коментар?';
$il['comments_list_header'] = 'Список коментарів';
$il['comments_select_range_header'] = 'Вибрати за період';
$il['comments_select_range_from'] = 'з';
$il['comments_select_range_to'] = 'по';
$il['comments_no_data_to_show'] = 'Немає даних для відображення';
$il['comments_pub_show_reviews'] = 'Показати відгуки';
$il['comments_reviews_menu_block'] = 'Відгуки';
$il['comments_reviews_list_header'] = 'Список відгуків';
$il['comments_property_pros_label'] = 'Переваги';
$il['comments_property_cons_label'] = 'Недоліки';
$il['comments_property_rate_label'] = 'Оцінка';
$il['comments_error_incorrect_datetime'] = 'Некоректна дата або час';
$il['comments_comment_form_header'] = 'Додати / Редагувати коментар';
$il['comments_review_form_header'] = 'Додати / Редагувати відгук';
$il['comments_pub_show_reviews_stat'] = 'Статистика за відгуками';