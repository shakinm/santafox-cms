<?php

class File {
  private $folder;

  public function __construct() {
    $this->folder = "cache/";
  }

  public function get($key) {
    $file_name = $this->fileName($key);    
    if (file_exists($file_name))
    {
        return file_get_contents($file_name);
    }

    return false;
  }

  public function set($key, $value) {
    $file_name = $this->fileName($key);    
    $file = fopen($file_name, "w+");
    fwrite($file, $value);
    fclose($file);
  }

  public function delete($key) {
    $file_name = $this->fileName($key);
    if (file_exists($file_name))
    {
        unlink($file_name);
    }
  }

  public function flush() {
    $caches = glob($this->folder . '*');
    if (count($caches) > 0) {
      foreach ($caches as $cache_file) {
        if (file_exists($cache_file)) {
          unlink($cache_file);
        }
      }
    }
  }

  private function fileName($file_name) {
    $key = md5($file_name);
    return $this->folder . $key . ".html";
  }
}
